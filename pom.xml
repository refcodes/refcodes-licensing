<?xml version="1.0" encoding="UTF-8"?>

<!-- ===========================================================================
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// ========================================================================= -->

<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">

	<modelVersion>4.0.0</modelVersion>

	<!-- /////////////////////////////////////////////////////////////////// -->
	<!-- ARTIFACT                                                            -->
	<!-- /////////////////////////////////////////////////////////////////// -->

	<groupId>org.refcodes</groupId>
	<artifactId>refcodes-licensing</artifactId>
	<version>3.4.0-SNAPSHOT</version>
	<name>${project.groupId}:${project.artifactId}</name>
	<packaging>jar</packaging>

	<!-- /////////////////////////////////////////////////////////////////// -->
	<!-- PROPERTIES                                                          -->
	<!-- /////////////////////////////////////////////////////////////////// -->
	
	<properties>
		<org.refcodes.url>http://www.refcodes.org</org.refcodes.url>
		<java.source.version>17</java.source.version>
		<java.target.version>17</java.target.version>
 		<maven.compiler.optimize>true</maven.compiler.optimize>
 		<maven.version>3.3.9</maven.version>
 		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
 		<org.apache.maven.plugins.assembly.version>3.6.0</org.apache.maven.plugins.assembly.version>
		<org.apache.maven.plugins.clean.version>3.3.1</org.apache.maven.plugins.clean.version>
		<org.apache.maven.plugins.compiler.version>3.11.0</org.apache.maven.plugins.compiler.version>
		<org.apache.maven.plugins.deploy.version>3.1.1</org.apache.maven.plugins.deploy.version>
		<org.apache.maven.plugins.enforcer.version>3.4.1</org.apache.maven.plugins.enforcer.version>
		<org.apache.maven.plugins.gpg.version>3.1.0</org.apache.maven.plugins.gpg.version>
		<org.apache.maven.plugins.install.version>3.1.1</org.apache.maven.plugins.install.version>
		<org.apache.maven.plugins.jar.version>3.3.0</org.apache.maven.plugins.jar.version>
		<org.apache.maven.plugins.javadoc.version>3.6.0</org.apache.maven.plugins.javadoc.version>
		<org.apache.maven.plugins.release.version>3.0.1</org.apache.maven.plugins.release.version>
		<org.apache.maven.plugins.resources.version>3.3.1</org.apache.maven.plugins.resources.version>
		<org.apache.maven.plugins.shade.version>3.5.0</org.apache.maven.plugins.shade.version>
		<org.apache.maven.plugins.site.version>3.12.1</org.apache.maven.plugins.site.version>
		<org.apache.maven.plugins.source.version>3.3.0</org.apache.maven.plugins.source.version> <!-- !!! Beware of 3.3.0 ?!? -->
		<org.apache.maven.plugins.surefire.version>3.1.2</org.apache.maven.plugins.surefire.version>
		<org.sonatype.plugins.nexus.staging.version>1.6.13</org.sonatype.plugins.nexus.staging.version>
	</properties>

	<!-- /////////////////////////////////////////////////////////////////// -->
	<!-- META-DATA                                                           -->
	<!-- /////////////////////////////////////////////////////////////////// -->
	
	<url>https://bitbucket.org/refcodes/${project.artifactId}</url>
	<description>
		The refcodes-licensing artifact is a meta-artifact included as a
		dependency into all artifacts which apply the herein contained licensing 
		terms (usually artifacts of the group org.refcodes). Them refcodes-
		licensing terms and conditions can be summarized as below. Please see
		the refcodes-licensing artifact of the version being applied to the
		artifact in question for the terms and conditions effectively being 
		applied.
	</description>

	<!-- /////////////////////////////////////////////////////////////////// -->
	<!-- LICENSES                                                            -->
	<!-- /////////////////////////////////////////////////////////////////// -->

	<licenses>
		<license>
			<name>Apache License, Version 2.0</name>
			<url>http://www.apache.org/licenses/LICENSE-2.0</url>
			<distribution>repo</distribution>
			<comments>See "http://en.wikipedia.org/wiki/Multi-licensing"</comments>
		</license>
		<license>
			<name>GNU General Public License (GPL), Version 3.0</name>
			<url>http://www.gnu.org/licenses/gpl-3.0.html</url>
			<distribution>repo</distribution>
			<comments>See "http://en.wikipedia.org/wiki/Multi-licensing"</comments>
		</license>
		<license>
			<name>Other License Agreement</name>
			<url>${org.refcodes.url}</url>
			<distribution>manual</distribution>
			<comments>
				Please contact the copyright holding author(s) of the software 
				artifacts in question for licensing issues not being covered by 
				the above listed licenses, also regarding commercial licensing 
				models or regarding the compatibility with other open source 
				licenses.
			</comments>
		</license>
	</licenses>

	<!-- /////////////////////////////////////////////////////////////////// -->
	<!-- DEVELOPERS                                                          -->
	<!-- /////////////////////////////////////////////////////////////////// -->

	<developers>
		<developer>
			<name>Siegfried Steiner</name>
			<email>steiner@refcodes.org</email>
			<url>http://www.refcodes.org</url>
		</developer>
	</developers>

	<!-- /////////////////////////////////////////////////////////////////// -->
	<!-- SCM                                                                 -->
	<!-- /////////////////////////////////////////////////////////////////// -->

	<scm>
		<connection>scm:git:git@bitbucket.org:refcodes/${project.artifactId}.git</connection>
		<developerConnection>scm:git:git@bitbucket.org:refcodes/${project.artifactId}.git</developerConnection>
		<url>https://bitbucket.org/refcodes/${project.artifactId}</url>
	</scm>

	<!-- /////////////////////////////////////////////////////////////////// -->
	<!-- DISTRIBUTION MANAGEMENT                                             -->
	<!-- /////////////////////////////////////////////////////////////////// -->
	
	<distributionManagement>
		<snapshotRepository>
			<id>ossrh</id>
			<url>https://s01.oss.sonatype.org/content/repositories/snapshots</url>
		</snapshotRepository>
		<repository>
			<id>ossrh</id>
			<url>https://s01.oss.sonatype.org/service/local/staging/deploy/maven2/</url>
		</repository>
	</distributionManagement>

	<!-- /////////////////////////////////////////////////////////////////// -->
	<!-- REPOSITORIES                                                        -->
	<!-- /////////////////////////////////////////////////////////////////// -->

	<repositories>
		<repository>
			<id>sonatype-nexus-snapshots</id>
			<name>Sonatype Nexus Snapshots</name>
			<url>https://s01.oss.sonatype.org/content/repositories/snapshots</url>
			<releases>
				<enabled>false</enabled>
			</releases>
			<snapshots>
				<enabled>true</enabled>
			</snapshots>
		</repository>
		<repository>
			<id>sonatype-nexus-releases</id>
			<name>Sonatype Nexus Releases</name>
			<url>https://s01.oss.sonatype.org/content/repositories/releases</url>
			<releases>
				<enabled>true</enabled>
			</releases>
			<snapshots>
				<enabled>false</enabled>
			</snapshots>
		</repository>
		<repository>
			<id>refcodes-snapshots</id>
			<name>REFCODES.ORG Snapshots</name>
			<releases>
				<enabled>false</enabled>
			</releases>
			<snapshots>
				<enabled>true</enabled>
			</snapshots>
			<!-- distributionManagement url was <url>git:your-branch://git@bitbucket.org:yourbitbucketusername/your-bitbucket-repo.git</url> -->
			<url>https://api.bitbucket.org/1.0/repositories/refcodes/refcodes-maven-repository/raw/snapshots</url>
		</repository>
		<repository>
			<id>refcodes-releases</id>
			<name>REFCODES.ORG Releases</name>
			<releases>
				<enabled>true</enabled>
			</releases>
			<snapshots>
				<enabled>false</enabled>
			</snapshots>
			<!-- distributionManagement url was <url>git:your-branch://git@bitbucket.org:yourbitbucketusername/your-bitbucket-repo.git</url> -->
			<url>https://api.bitbucket.org/1.0/repositories/refcodes/refcodes-maven-repository/raw/releases</url>
		</repository>
	</repositories>

	<!-- /////////////////////////////////////////////////////////////////// -->
	<!-- BUILD                                                               -->
	<!-- /////////////////////////////////////////////////////////////////// -->

	<build>

		<!-- /////////////////////////////////////////////////////////////// -->
		<!-- PLUGIN MANAGEMENT                                               -->
		<!-- /////////////////////////////////////////////////////////////// -->

		<pluginManagement>
			<plugins>
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-enforcer-plugin</artifactId>
					<version>${org.apache.maven.plugins.enforcer.version}</version>
				</plugin>
				<plugin>
				    <groupId>org.apache.maven.plugins</groupId>
				    <artifactId>maven-clean-plugin</artifactId>
				    <version>${org.apache.maven.plugins.clean.version}</version>
				</plugin>
				<plugin>
				    <groupId>org.apache.maven.plugins</groupId>
				    <artifactId>maven-install-plugin</artifactId>
				    <version>${org.apache.maven.plugins.install.version}</version>
				</plugin>
				<plugin>
				    <groupId>org.apache.maven.plugins</groupId>
				    <artifactId>maven-resources-plugin</artifactId>
				    <version>${org.apache.maven.plugins.resources.version}</version>
				</plugin>
				<plugin>
				    <groupId>org.apache.maven.plugins</groupId>
				    <artifactId>maven-site-plugin</artifactId>
				    <version>${org.apache.maven.plugins.site.version}</version>
				</plugin>
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
				    <artifactId>maven-deploy-plugin</artifactId>
				    <version>${org.apache.maven.plugins.deploy.version}</version>
				</plugin>
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-source-plugin</artifactId>
					<version>${org.apache.maven.plugins.source.version}</version>
				</plugin>
				<plugin>
					<artifactId>maven-compiler-plugin</artifactId>
					<version>${org.apache.maven.plugins.compiler.version}</version>
					<configuration>
						<fork>true</fork>
						<source>${java.source.version}</source>
						<target>${java.target.version}</target>
						<testSource>${java.source.version}</testSource>
                    	<testTarget>${java.target.version}</testTarget>
                    	<encoding>UTF-8</encoding>
					</configuration>
				</plugin>
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-surefire-plugin</artifactId>
					<version>${org.apache.maven.plugins.surefire.version}</version>
				</plugin>
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-jar-plugin</artifactId>
					<version>${org.apache.maven.plugins.jar.version}</version>
				</plugin>
			</plugins>
		</pluginManagement>

		<!-- /////////////////////////////////////////////////////////////// -->
		<!-- PLUGINS                                                         -->
		<!-- /////////////////////////////////////////////////////////////// -->

		<plugins>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-enforcer-plugin</artifactId>
				<executions>
					<execution>
						<id>enforce-versions</id>
						<goals>
							<goal>enforce</goal>
						</goals>
						<configuration>
							<rules>
								<requireMavenVersion>
									<version>${maven.version}</version>
								</requireMavenVersion>
								<requireJavaVersion>
									<version>${java.target.version}</version>
								</requireJavaVersion>
							</rules>
						</configuration>
					</execution>
				</executions>
			</plugin>
			<plugin>
				<artifactId>maven-surefire-plugin</artifactId>
				<configuration>
					<includes>
						<include>**/*Test.java</include>
					</includes>
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-javadoc-plugin</artifactId>
				<version>${org.apache.maven.plugins.javadoc.version}</version>
				<configuration>
					<source>9</source>
					<additionalparam>--html5</additionalparam>
					<additionalparam>--verbose</additionalparam>
					<!-- ////////////////////////////////// -->
					<!-- Workaround for ugly javadoc bug(?) -->
					<!-- ////////////////////////////////// -->
					<sourceFileExcludes>
						<sourceFileExclude>/module-info.java</sourceFileExclude>
					</sourceFileExcludes>
					<!-- ////////////////////////////////// -->
					<detectLinks>true</detectLinks>
					<detectJavaApiLink>true</detectJavaApiLink>
					<!-- ////////////////////////////////////////////////////////////////////////////////////////////////////// -->
					<!-- "https://stackoverflow.com/questions/39616344/how-to-disable-javadoc-warnings-in-maven-javadoc-plugin" -->
					<!-- ////////////////////////////////////////////////////////////////////////////////////////////////////// -->
					<additionalparam>-Xdoclint:all -Xdoclint:-missing</additionalparam>
					<additionalOptions>-Xdoclint:all -Xdoclint:-missing</additionalOptions>
					<additionalJOptions>
						<additionalJOption>-Xdoclint:all</additionalJOption>
						<additionalJOption>-Xdoclint:-missing</additionalJOption>
						<additionalJOption>-Xdoclint:none</additionalJOption>
					</additionalJOptions>
					<!-- ////////////////////////////////////////////////////////////////////////////////////////////////////// -->
				</configuration>
			</plugin>
			<plugin>
				<artifactId>maven-compiler-plugin</artifactId>
				<configuration>
					<fork>true</fork>
					<source>${java.source.version}</source>
					<target>${java.target.version}</target>
					<testSource>${java.source.version}</testSource>
					<testTarget>${java.target.version}</testTarget>
					<encoding>UTF-8</encoding>
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-jar-plugin</artifactId>
				<configuration>
					<archive>
						<addMavenDescriptor>false</addMavenDescriptor>
						<manifest>
							<addDefaultSpecificationEntries>true</addDefaultSpecificationEntries>
							<addDefaultImplementationEntries>true</addDefaultImplementationEntries>
						</manifest>
						<manifestEntries>
							<Automatic-Module-Name>org.refcodes.licensing</Automatic-Module-Name>
						</manifestEntries>
					</archive>
				</configuration>
				<executions>
					<execution>
						<goals>
							<goal>test-jar</goal>
						</goals>
					</execution>
				</executions>
			</plugin>
			<plugin>
				<groupId>org.sonatype.plugins</groupId>
				<artifactId>nexus-staging-maven-plugin</artifactId>
				<version>${org.sonatype.plugins.nexus.staging.version}</version>
				<extensions>true</extensions>
				<configuration>
					<serverId>ossrh</serverId>
					<nexusUrl>https://s01.oss.sonatype.org/</nexusUrl>
					<autoReleaseAfterClose>true</autoReleaseAfterClose>
				</configuration>
			</plugin>
		</plugins>
	</build>

	<!-- /////////////////////////////////////////////////////////////////// -->
	<!-- PROFILES                                                            -->
	<!-- /////////////////////////////////////////////////////////////////// -->

	<profiles>
		<profile> 
			<id>release</id>
			<build>
				<plugins>
					<plugin>
						<groupId>org.apache.maven.plugins</groupId>
						<artifactId>maven-source-plugin</artifactId>
						<executions>
							<execution>
								<id>attach-sources</id>
								<goals>
									<goal>jar-no-fork</goal>
								</goals>
							</execution>
						</executions>
					</plugin>
					<plugin>
						<groupId>org.apache.maven.plugins</groupId>
						<artifactId>maven-javadoc-plugin</artifactId>
						<executions>
							<execution>
								<id>attach-javadocs</id>
								<goals>
									<goal>jar</goal>
								</goals>
							</execution>
						</executions>
					</plugin>
					<plugin>
						<groupId>org.apache.maven.plugins</groupId>
						<artifactId>maven-gpg-plugin</artifactId>
						<executions>
							<execution>
								<id>sign-artifacts</id>
								<phase>verify</phase>
								<goals>
									<goal>sign</goal>
								</goals>
							</execution>
						</executions>
					</plugin>
				</plugins>
			</build>
		</profile>
	</profiles>
</project>
