# README #

> The [`REFCODES.ORG`](http://www.refcodes.org/refcodes) codes represent a group of artifacts consolidating parts of my work in the past years. Several topics are covered which I consider useful for you, programmers, developers and software engineers.

Therefore [`REFCODES.ORG`](http://www.refcodes.org/refcodes) ([`org.refcodes`](https://bitbucket.org/refcodes) group) group of artifacts is published under some open source licenses; covered by this artifact:

Each artifact available under the below licensing agreements includes this [`refcodes-licensing`](https://bitbucket.org/refcodes/refcodes-licensing) ([`org.refcodes`](https://bitbucket.org/refcodes) group) artifact in its maven (gradle) artifact dependency - for example in the artifact's `pom.xml`.

## What is this repository for? ##

***The [`refcodes-licensing`](https://bitbucket.org/refcodes/refcodes-licensing)  artifact is a meta-artifact included as a dependency into all artifacts which apply the herein contained licensing terms (usually artifacts of the group [`org.refcodes`](https://bitbucket.org/refcodes)). Them  [`refcodes-licensing`](https://bitbucket.org/refcodes/refcodes-licensing) terms and conditions can be summarized as below. Please see the [`refcodes-licensing`](https://bitbucket.org/refcodes/refcodes-licensing) artifact of the version being applied to the artifact in question for the terms and conditions effectively being applied.***

#### REFCODES.ORG ####

Below find the most current [`REFCODES.ORG`](http://www.refcodes.org/refcodes) terms and conditions as of the time of this writing (2015-01-27). 

##### Licensing terms and conditions #####

    /////////////////////////////////////////////////////////////////////////////
    REFCODES.ORG
    =============================================================================
    This code is copyright (c) by Siegfried Steiner, Munich, Germany and licensed
    under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
    licenses:
    =============================================================================
    GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
    together with the GPL linking exception applied; as applied by the GNU Classpath
    ("http://www.gnu.org/software/classpath/license.html")
    =============================================================================
    Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
    =============================================================================
    Please contact the copyright holding author(s) of the software artifacts in
    question for licensing issues not being covered by the above listed licenses,
    also regarding commercial licensing models or regarding the compatibility
    with other open source licenses.
    /////////////////////////////////////////////////////////////////////////////

## How do I get set up? ##

In case you work on [`REFCODES.ORG`](http://www.refcodes.org/refcodes) artifacts, then include this artifact as a dependency in your artifact (or one of its parents) as follows:

```
<dependency>
	<groupId>org.refcodes</groupId>
	<artifactId>refcodes-licensing</artifactId>
	<version>3.0.0</version>
</dependency>
```

The artifact is hosted directly at [Maven Central](http://search.maven.org). Jump straight to the source codes at [Bitbucket](https://bitbucket.org/refcodes/refcodes-licensing). Read the artifact's javadoc at [javadoc.io](http://www.javadoc.io/doc/org.refcodes/refcodes-licensing).

I consider that the license agreement is inherited from a parent artifact to its children in case not otherwise (and compliant to the [`refcodes-licensing`](https://bitbucket.org/refcodes/refcodes-licensing) terms and conditions) stated. 

## Contribution guidelines ##


* [Report issues](https://bitbucket.org/refcodes/refcodes-licensing/issues)
* Finding bugs
* Helping fixing bugs
* Making code and documentation better
* Enhance the code

## Who do I talk to? ##

* Siegfried Steiner (steiner@refcodes.org)
## Terms and conditions ##

The [`REFCODES.ORG`](http://www.refcodes.org/refcodes) group of artifacts is published under some open source licenses; covered by the  [`refcodes-licensing`](https://bitbucket.org/refcodes/refcodes-licensing) ([`org.refcodes`](https://bitbucket.org/refcodes) group) artifact - evident in each artifact in question as of the `pom.xml` dependency included in such artifact.
